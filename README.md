# README
This tutorial represents an example on how to integrate the mARGOt framework in a target application.
In particular, in this tutorial we will integrate mARGOt in a toy application that exposes a single software knob (num_trial).
The application requirements aims at minimizing the elaboration error given a constraint on the execution time.

The source file of the original application is "src/original_file.cpp", while the source file of the application after the integration with mARGOt is "src/main.cpp". We will compile the latter file, the former is for highlighting the integration code.

In the remainder of the tutorial, we make the assumption that you have already read the documentation of the mARGOt framework and would like to have an example of a workflow.


## Installation Instruction

The first step is to clone the mARGOt framework from the repository:
~~~
:::bash
$ git clone https://gitlab.com/margot_project/core.git
~~~

The readme of the previous repository states all the build instructions. From an implementation point of view, the mARGOt framework is a C++ library to be linked against the target application.

To test if we have correctly compiled the library, it is possible to enable the generation of an executable that performs tests of the framework. If it passes all the tests, you have correctly built the library.

The next step is to build the high-level interface, according to application requirements, that eases the integration process, hiding implementation details.

Assuming that you have cloned the mARGOt framework in the folder "./core":

1) Get a copy of the template of the high-level interface.
~~~
:::bash
	$ cp -r core/margot_heel/margot_heel_if/ .
~~~

2) Delete its default configuration files.
~~~
:::bash
	$ rm margot_heel_if/config/*.conf
~~~

3) Copy the actual configuration files of this application in the high-level interface
~~~
:::bash
	$ cp config/*.conf margot_heel_if/config/
~~~

The key idea is to leverage "margot_cli" to generate an high-level interface that eases the integration process.
In particular, the configuration files describe all the extra-functional requirements for the target application in a simple XML file.

4) Generate and compile the high-level interface.
~~~
:::bash
	$ cd margot_heel_if/
	$ mkdir build
	$ cd build
	$ cmake ..
	$ make
~~~

At this point we have built the mARGOt library and the mARGOt heel library, which is an high-level interface tailored for this specific application.
In the remainder of the tutorial we just compile the toy application, linking the mARGOt libraries. For this reason, you may find a FindMARGOT_HELL.cmake file in "./margot_heel_if/cmake" and a margot_heel.pc file in "./margot_heel_if/build".
The tutorial uses the cmake build system.

5) The last step is to build the actual application.
~~~
:::bash
	$ cd ../..
	$ mkdir build
	$ cd build
	$ cmake ..
	$ make
~~~
