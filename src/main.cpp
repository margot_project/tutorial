#include <margot.hpp>
#include <chrono>
#include <thread>
/*

 The function that will be managed by margot must already be a tunable function

 */
void do_work (int trials)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(trials));
}

int main()
{
  margot::init();
  int trials = 1;
  int repetitions = 10000;
  for (int i = 0; i<repetitions; ++i)
  {
    //check if the configuration is different wrt the previous one
    //NOTE: trials is an output parameter!
    if (margot::foo::update(trials))
    {
      // optional: some code to apply chosen config here
      //
      //Writing the "trials" variable is enough to change configuration
      margot::foo::manager.configuration_applied();
    }
    //monitors wrap the autotuned function
    margot::foo::start_monitor();
    do_work(trials);
    margot::foo::stop_monitor(3.4f);
    //print values to file
    margot::foo::log();
	}



}
